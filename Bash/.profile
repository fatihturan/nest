export PS1="\[\e[1;32m\]\h:\[\e[m\]\[\e[0;34m\]\W\[\e[m\] \[\e[1;33m\]\u\[\e[m\]\[\e[0;33m\]\$\[\e[m\] "
# export PS1="\h →"
alias ..='cd ..'
alias ...='cd ../..'
alias cd..='cd ..'
alias ll='ls -alF'
alias la='ls -lah'
alias l='ls -l'
alias ls='ls $LS_OPTIONS'
alias ls-l='ls -l'
alias cl='clear'
alias glog="git log --format='%Cgreen%h%Creset %C(cyan)%an%Creset - %s' --graph"


export LC_ALL=tr_TR.utf8
